envelopes.pdf: envelopes.tex Makefile
	/usr/bin/pdflatex -halt-on-error envelopes.tex

clean:
	/usr/bin/rm -f *.aux *.log *.toc
