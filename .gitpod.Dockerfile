FROM gitpod/workspace-full

# Update system, install LaTeX and helper tools
RUN sudo apt-get -q update && \
  sudo apt-get upgrade -y &&\
  sudo apt-get install -yq texlive && \
  sudo rm -rf /var/lib/apt/lists/*
