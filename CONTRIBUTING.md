# Contributing

This is just a MWE to get people started. I'm not really planning on keeping this repository constantly updated because I only need to use it once a year, but if there are issues with the packages, I will try my best to keep this working for people.

If you find any issues, please submit an issue (and hopefully a PR!).
