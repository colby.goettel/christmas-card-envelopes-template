# Christmas card envelopes

This repository contains a minimum working example (MWE) for how to print envelopes using LaTeX. It is setup to use A7 envelopes (7.25"x5.25") which is the standard for Christmas, holiday, wedding, and birthday invitations.

Feel free to fork this repository and use it. If you run into any issues or would like to contribute, see [the contributing doc](CONTRIBUTING.md).

## Usage

To simplify usage, I have provided a Gitpod image so you don't need to have LaTeX installed locally. Just spin up this repository in Gitpod, make your changes, run `make`, and commit the new PDF.

Because this is a template, you will need to change a few values in `envelopes.tex`:
- Note that in LaTeX, two backslashes (`\\`) represent a newline. You will use that as a line separator for addresses.
- I'm assuming you want to print A7 envelopes (7.25"x5.25"). If you need a different envelope size, you will need to change the `\envelopeheight` and `\envelopewidth` variables at the top of the file.
- You will need to put your address into the line beginning `\renewcommand*{\fromaddress}`.
- Scroll down to the bottom. You will see where the document begins (`\begin{document}`). Within the document, there are multiple lines that start with `\mlabel`. Each of these lines represent one envelope. The `\mlabel` command requires two arguments. In LaTeX, arguments are supplied within brackets. The first argument is the return address. The second argument is the name and address of the person you want to send something to. Create as many `\mlabel` entries as you need, following the examples provided.

When you're done editing `envelopes.tex` (or want to verify that things are working), run `make` and `envelopes.pdf` will be generated. You should expect the last two lines of output to be something like:
```
Output written on envelopes.pdf (2 pages, 46444 bytes).
Transcript written on envelopes.log.
```
If you don't see that, there was an issue. The last part of the output should tell you what's wrong.

NOTE: If you are using Gitpod, you may not be able to view the PDF through your IDE. In this case, you will need to commit and push your code so that you can view the PDF. Sorry. It sucks, I know, and hopefully they're working on fixing it.

## Further reading

The envlab package, by default, prints the address in an OCR-capable font and includes a barcode for the USPS' automated processing equipment. More information can be found at these links:
- [envlab package readme](https://ctan.math.washington.edu/tex-archive/macros/latex/contrib/envlab/envlab.pdf)
- [USPS information](https://pe.usps.com/businessmail101?ViewName=AutoLetters)
